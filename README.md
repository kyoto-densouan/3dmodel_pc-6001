# README #

1/3スケールのNEC PC-6001風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK Fusion360です。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1981年11月10日

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-6000%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA#PC-6001)
- [「僧兵ちまちま」のゲーム日記。](https://blog.goo.ne.jp/timatima-psu/d/20180509)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001/raw/0f32fb0218e76ca94959645c82a57dde4fd8c24a/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001/raw/0f32fb0218e76ca94959645c82a57dde4fd8c24a/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-6001/raw/0f32fb0218e76ca94959645c82a57dde4fd8c24a/ExampleImage.png)
